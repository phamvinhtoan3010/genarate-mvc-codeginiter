<!DOCTYPE html>

<head>
    <!-- *** METADATA *** -->
    <meta name="copyright" content="&copy; Copyright 2011 StevenFlesch.com" />
    <meta charset="UTF-8">
    <title>CI gen</title>
    <!-- *** CSS *** -->
    <!-- <link rel="stylesheet" href="css/table2class.css" type="text/css" media="screen"> -->
    <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px;
    }
    div#content-right {
	font-family: monospace;
	text-align: left;
	float: left;
    }
    div#output {
	font-family: monospace;
	text-align: left;
    float: left;
    color: green;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
   
}

  </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript">
        // Bind form elements to actions
        $(document).ready(function() {
            // Connect button
            $("#serverconnect").click(function() {
                // Reset form
                $("#database").attr("disabled", "disabled");
                $("#database").html("<option>... Please Connect ...</option>");
                $("#table").attr("disabled", "disabled");
                $("#table").html("<option>... Please Connect ...</option>");
                $("#keyfield").attr("disabled", "disabled");
                $("#keyfield").html("<option>... Please Connect ...</option>");
                $("#classname").attr("disabled", "disabled");
                $("#classname").attr("value", "");
                $("#classgenerate").attr("disabled", "disabled");
                // Output message
                $("#output").html("Attempting to connect to MySQL server...");

                // Submit form
                $.post("includes/ajax.showdatabases.php", $("#form-connect").serialize(),
                    function(data) {
                        if (data.indexOf("Error:") < 0) {
                            // Load output into select box for databases.
                            $("#database").html(data);
                            $("#database").removeAttr("disabled").focus(); // enable form
                            $("#output").html($("#output").html() + " done.");
                        } else {
                            // No data outputted, no valid databases.
                            $("#output").html($("#output").html() + "</br>"+ data);
                        }
                    }, "html");
                return false; // prevent traditional submit
            });

            // Database selectbox
            $("#database").change(function() {
                // Disable lower parts of form.
                $("#table").attr("disabled", "disabled");
                $("#table").html("<option>... Please Connect ...</option>");
                $("#keyfield").attr("disabled", "disabled");
                $("#keyfield").html("<option>... Please Connect ...</option>");
                $("#classname").attr("disabled", "disabled");
                $("#classname").attr("value", "");
                $("#classgenerate").attr("disabled", "disabled");

                // Output text
                var strDatabase = $("#database option:selected").text();
                if (strDatabase) {
                    $("#output").html("Retrieving tables for database \"" + strDatabase + "\"...");
                    // Populate selectbox
                    $.post("includes/ajax.showtables.php", $("#form-connect").serialize(),
                        function(data) {
                            if (data.indexOf("Error:") < 0) {
                                // Load output into select box for databases.
                                $("#table").html(data);
                                $("#table").removeAttr("disabled").focus(); // enable form
                                $("#output").html($("#output").html() + " done.");
                            } else {
                                // No data outputted, no valid databases.
                                $("#output").html($("#output").html() + "</br>"+ data);
                            }
                        }, "html");
                    return false; // prevent traditional submit
                }
            });

            // Table selectbox
            $("#table").change(function() {
                // Disable lower parts of the form.
                $("#keyfield").attr("disabled", "disabled");
                $("#keyfield").html("<option>... Please Connect ...</option>");
                $("#classname").attr("disabled", "disabled");
                $("#classname").attr("value", "");
                $("#classgenerate").attr("disabled", "disabled");

                // Output text
                var strTable = $("#table option:selected").text();
                if (strTable) {
                    $("#output").html("Retrieving columns for table \"" + strTable + "\"...");
                    // Populate selectbox
                    $.post("includes/ajax.showcolumns.php", $("#form-connect").serialize(),
                        function(data) {
                            if (data.indexOf("Error:") < 0) {
                                // Load output into select box for databases.
                                $("#keyfield").html(data);
                                $("#keyfield").removeAttr("disabled").focus(); // enable form
                                $("#output").html($("#output").html() + " done.");
                            } else {
                                // No data outputted, no valid databases.
                                $("#output").html($("#output").html() + "</br>"+ data);
                            }
                        }, "html");
                    return false; // prevent traditional submit
                }
            });

            // Keyfield selectbox
            $("#keyfield").change(function() {
                // Output text
                var strClassname = $("#table option:selected").text();
                // Populate classname field.
                $("#classname").attr("value", strClassname);
                //$("#classname").text(strKeyfield);
                $("#classgenerate").removeAttr("disabled");
                $("#classname").removeAttr("disabled").focus();
            });

            // Generate Class Button
            $("#classgenerate").click(function() {
                // Output text
                var strClassname = $("#classname").attr("value");
                var strFilename = "class." + strClassname + ".php";
                $("#output").html("Generating class file for class \"" + strClassname + "\"...");
                // Post results to createfile page.
                $.post("includes/ajax.createfile.php", $("#form-connect").serialize(),
                    function(data) {
                        if (data.indexOf("Error:") < 0) {
                            $("#output").html($("#output").html() + " done.<br /><br />" + data);
                        } else {
                            // No data outputted, no valid databases.
                            $("#output").html($("#output").html() + "</br>"+ data);
                        }
                    }, "html");
                return false;
            });
        });
    </script>
</head>

<body>
<div class="container-fluid">
    <div class="content">
        <form id="form-connect" method="post">
            <div class="col-sm-3 sidenav">
                <h2>Connect To Your Server:</h2>
                <div id="column-outer" class="form-group">
                    <div id="column-inner" class="form-group">
                        <div id="content-left" class="form-group">
                            <!-- Encase all options in this form -->
                            <div id="div-connect" class="form-group">
                                <div class="form-group">
                                    <label for="email">Server Address</label>
                                    <input type="text" name="serveraddress" type="text" value="localhost" class="form-control" id="serveraddress">
                                </div>
                                <div class="form-group">
                                    <label for="email">Server Username</label>
                                    <input type="text" name="serverusername" type="text" value="root" class="form-control" id="serverusername">
                                </div>
                                <div class="form-group">
                                    <label for="email">Server Password</label>
                                    <input type="text" name="serverpassword" class="form-control" id="serverpassword" placeholder="Enter password">
                                </div>
                                <button name="serverconnect" id="serverconnect" class="btn btn-default">Connect</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" id="content-right" class="form-group">
                <h2>Connect To Your database:</h2>
            
                <label for="email">Select Database</label>
                
                <div id="div-database" class="form-group">
                    <select name="database" id="database" size="1" class="form-control" disabled="disabled">
                        <option>... Please Connect ...</option>
                    </select>
                </div>
                <label for="email">Select Table</label>
                
                <div id="div-table" class="form-group">
                    <select name="table" id="table" size="1" class="form-control" disabled="disabled">
                        <option>... Please Select Database ...</option>
                    </select>
                </div>
                <label for="email">Select Primary Key</label>

                <div id="div-key" class="form-group">
                    <select name="keyfield" id="keyfield" class="form-control" size="1" disabled="disabled">
                        <option>... Please Select Primary Key ...</option>
                    </select>
                </div>
                <label for="email">Select Class Options</label>
                <div id="div-class" class="form-group">
                    <label for="classname">Class Name</label>
                    <br />
                    <input name="classname" id="classname" class="form-control" class="bold center" type="te" disabled="disabled" />
                </div>
                <div  class="form-group">
                    <button name="classgenerate" id="classgenerate" class="btn btn-primary" disabled="disabled">Generate Class</button>
                </div>
            </div>            
            <div class="col-sm-6" id="content-right" >
                <h2>Script Output</h2>
                <div id="output" >
                
                </div>
            </div>
        </form>        
    </div>
</div>
<!-- <footer class="container-fluid">
  <p>This project is referenced from table2class</p>
</footer> -->
</body>

</html>